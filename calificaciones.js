function obtenerParrafos() {
  return document.getElementsByTagName("p");
var tabla_calificaciones = document.getElementById('tabla_calificaciones');
var cuerpo_tabla = tabla_calificaciones.children[1];

var MateriaCalificacion = function(_mat,_calif) {
	return {
		"materia": _mat,
		"calificacion": _calif
	};
 }
 
function obtenerParrafoPorId(id) {
  return document.getElementById(id);
function obtenerMaterias(){
	var materia;
	var materias = [];
    for(var i = 0; i < cuerpo_tabla.children.length; i){
		materia = MateriaCalificacion(cuerpo_tabla.children[i].children[1].textContent,Number(cuerpo_tabla.children[i].children[2].textContent));
        materias.push(materia);
    }
    return materias;
 }
 
function obtenerElementosDeLista() {
  return document.getElementsByTagName("li");
var mats = obtenerMaterias();

function calcularPromedio() {
    var total = 0;
    for(var i = 0; i < mats.length; i){
        total = mats[i].calificacion;
    }
    var promedio = total / mats.length;
    return promedio;
 }

function materiaMasAlta() {
	var materia_ma = mats[0].materia;
	var calificacion_ma = mats[0].calificacion;
	for(var i = 0;i < (mats.length  1); i){
		if(mats[i1].calificacion > calificacion_ma){
			materia_ma = mats[i1].materia;
			calificacion_ma = mats[i1].calificacion;
		}	
	}
	return materia_ma;
}

function materiaMasBaja() {
	var materia_mb = mats[0].materia;
	var calificacion_mb = mats[0].calificacion;
	for(var i = 0;i < (mats.length  1); i){
		if(mats[i1].calificacion < calificacion_mb){
			materia_mb = mats[i1].materia;
			calificacion_mb = mats[i1].calificacion;
		}	
	}
	return materia_mb;
}

function mostrarResultados(){
  var listaResultados = document.getElementById('lista_resultados');
  listaResultados.children[0].textContent = "Promedio: "  calcularPromedio();
  listaResultados.children[1].textContent = "Materia con la calificación más alta: "  materiaMasAlta();  
  listaResultados.children[2].textContent = "Materia con la calificación más baja: "  materiaMasBaja();   
}